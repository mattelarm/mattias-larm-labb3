//
//  MyTableViewController.m
//  Labb3Test
//
//  Created by IT-Högskolan on 2015-02-09.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "MyTableViewController.h"
#import "MyDetailViewController.h"
#import "AddThingTodoViewController.h"
#import "Task.h"

@interface MyTableViewController ()
@property (nonatomic) NSMutableArray *thingsTodo;

@end

@implementation MyTableViewController



-(NSMutableArray*)thingsTodo {
    if (!_thingsTodo) {
        
        Task *t1 = [[Task alloc]init];
        t1.name = @"Städa";
        
        Task *t2 = [[Task alloc]init];
        t2.name = @"Diska";
        
        Task *t3 = [[Task alloc]init];
        t3.name = @"Tvätta";
        
        Task *t4 = [[Task alloc]init];
        t4.name = @"Dricka Öl";
        
        Task *t5 = [[Task alloc]init];
        t5.name = @"Programmera";
        
        self.thingsTodo = [@[t1.name,t2.name,t3.name,t4.name,t5.name]mutableCopy];
        }
        return _thingsTodo;
    }

- (IBAction)SaveButtonPressed:(id)sender {
    NSUserDefaults *save = [NSUserDefaults standardUserDefaults];
    [save setObject:_thingsTodo forKey:@"myArray"];
    [save synchronize];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    
   // NSUserDefaults *save = [NSUserDefaults standardUserDefaults];
   // _thingsTodo = [save objectForKey:@"myArray"];
    
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.thingsTodo.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * cellIdentifier = @"MyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = self.thingsTodo[indexPath.row];
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
         [_thingsTodo removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"Detail"]) {
    MyDetailViewController *detailView = [segue destinationViewController];
    UITableViewCell *cell = sender;
    detailView.title = cell.textLabel.text;
    }
    else if ([segue.identifier isEqualToString:@"Add"]) {
        AddThingTodoViewController *addView = [segue destinationViewController];
        addView.thingsTodo = self.thingsTodo;
    }
    else {
        NSLog(@"You forgot the segue %@", segue);
    }
    
}


@end
