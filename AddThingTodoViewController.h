//
//  AddThingTodoViewController.h
//  Labb3Test
//
//  Created by IT-Högskolan on 2015-02-09.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddThingTodoViewController : UIViewController

@property (nonatomic) NSMutableArray *thingsTodo;

@end
